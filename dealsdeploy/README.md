# Designs approachs for a car(deal) bidding website

## Key Features
- multiple sources for deal
- Showing rooms live
- Bidding rooms live

## Key Actors

- Moderator (manager)
- Moderator (worker)
- Buyer
- Seller

## Extra

- [Diagrams ](https://miro.com/app/board/uXjVOMLg9Zc=/?invite_link_id=858442044721)

## Code
### Frontend 
- [Frontend](https://bitbucket.org/qobbysam/dealsfrontendbase/)
### Backend
- [appbrain](https://bitbucket.org/qobbysam/dealbase/)
- [fileserver](https://bitbucket.org/qobbysam/myauth/)
- [authserver](https://bitbucket.org/qobbysam/myauth/)






