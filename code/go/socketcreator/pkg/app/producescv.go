package app

type CSVLoaded struct {
	Email  string
	Fields []FieldNameValue
}

type FieldNameValue struct {
	FieldName string
	Value     string
}

type LoadedSMTP struct {
	ToAddress string
	Template  string
	SmtpInfo  SmtpInfo
}

type SmtpInfo struct {
	Type       string `json:"type"`
	Host       string `json:"host"`
	User       string `json:"username"`
	Password   string `json:"password"`
	SendAdress string `json:"sendaddress"`
	Port       string `json:"port"`
}

// type FinalWritable struct {
// 	Subject   string   `json:"subject"`
// 	Template  string   `json:"template"`
// 	SmtpInfo  SmtpInfo `json:"smtpinfo"`
// 	ToAddress string   `json:"toaddress"`
// 	ReplyTo   string   `json:"replyto"`
// }

type FinalWritable struct {
	ID           string   `json:"id"`
	Sender       string   `json:"sender"`
	To           []string `json:"tolist"`
	SmtpUser     string   `json:"smtpuser"`
	SmtpPassword string   `json:"smtppassword"`
	SmtpPort     string   `json:"smtpport"`
	SmtpHost     string   `json:"smtphost"`
	Subject      string   `json:"subject"`
	Body         string   `json:"body"`
	ContentType  string   `json:"contenttype"`
}
