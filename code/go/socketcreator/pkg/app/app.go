package app

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/lithammer/shortuuid"
	"github.com/qobbysam/socketcreator/pkg/config"
)

type Contact struct {
	Email  string
	Others []string
}

var TemplateString string = ` `

type App struct {
	Template    string
	ContentType string
	Subject     string
	KeysContact map[string]int
	FieldBinds  map[string]string
	SmtpPool    []SmtpInfo
	Bindables   []CSVLoaded

	Cfg *config.BigConfig
}

func (a *App) LoadCsv() error {
	//make sure line one has field names

	f, _ := os.Executable()

	dir := filepath.Dir(f)

	path_to_csv := filepath.Join(dir, "data", "indata")
	log.Println("getting sending list")
	log.Println(a.Cfg.Sending)
	file_to_load := filepath.Join(path_to_csv, a.Cfg.Sending.ListName)

	fi, err := os.Open(file_to_load)

	if err != nil {

		log.Println("failed to open file here")
		log.Println(err)

		return err
	}

	// by, err := ioutil.ReadAll(fi)

	// if err != nil {
	// 	fmt.Println("failed read file ,  ", err)

	// }

	csvReader := csv.NewReader(fi)
	data, err := csvReader.ReadAll()
	if err != nil {
		log.Fatal(err)
		return err
	}

	good_consume := make([]CSVLoaded, 0)

	for k, v := range data {

		if k == 0 {
			a.FindKeyLocation(v)
		}

		if k > 0 {

			good_consume = append(good_consume, a.BuildCsvLoaded(v))
		}
	}

	a.Bindables = good_consume

	log.Println(a.Bindables)

	return err

}

func (a *App) BuildCsvLoaded(list []string) CSVLoaded {

	email := ""

	fields := make([]FieldNameValue, 0)

	for k, v := range a.KeysContact {

		if k == "EMAIL" {

			email = list[v]
		} else {
			var f FieldNameValue

			f.FieldName = k
			f.Value = list[v]

			fields = append(fields, f)
		}

	}

	out := CSVLoaded{
		Email:  email,
		Fields: fields,
	}

	return out

}

func (a *App) LoadTemplate() error {
	f, _ := os.Executable()

	dir := filepath.Dir(f)

	path_to_tem := filepath.Join(dir, "data", "templates")
	log.Println("getting sending list")
	log.Println(a.Cfg.Sending.TemplateName)

	file_to_load := filepath.Join(path_to_tem, a.Cfg.Sending.TemplateName)

	fi, err := os.Open(file_to_load)

	if err != nil {

		log.Println("failed to open file here")
		log.Println(err)

		return err
	}

	data, err := ioutil.ReadAll(fi)

	raw_string := string(data)

	log.Println(raw_string)

	log.Println(a.SmtpPool)

	a.Template = raw_string

	return err
}

func (a *App) FindKeyLocation(list []string) {

	goodkeys := make(map[string]int)

	for _, v := range a.Cfg.ContactFields {
		upper := strings.ToUpper(v)
		goodkeys[upper] = -1
	}

	for k, v := range list {

		upper_ := strings.ToUpper(v)
		val, ok := goodkeys[upper_]

		if !ok {
			continue
		} else {
			if val == -1 {
				goodkeys[upper_] = k
			} else {
				continue
			}

		}
	}

	a.KeysContact = goodkeys

}

func (a *App) MakeFieldMaps() {

	val := make(map[string]string)

	// for i := 1; i <= 10; i++ {
	// 	name := "field" + strconv.Itoa(i)

	// }

	val["field1"] = strings.ToUpper(a.Cfg.TemplateFields.Field1)
	val["field2"] = strings.ToUpper(a.Cfg.TemplateFields.Field2)
	val["field3"] = strings.ToUpper(a.Cfg.TemplateFields.Field3)
	val["field4"] = strings.ToUpper(a.Cfg.TemplateFields.Field4)
	val["field5"] = strings.ToUpper(a.Cfg.TemplateFields.Field5)
	val["field6"] = strings.ToUpper(a.Cfg.TemplateFields.Field6)
	val["field7"] = strings.ToUpper(a.Cfg.TemplateFields.Field7)
	val["field8"] = strings.ToUpper(a.Cfg.TemplateFields.Field8)
	val["field9"] = strings.ToUpper(a.Cfg.TemplateFields.Field9)
	val["field10"] = strings.ToUpper(a.Cfg.TemplateFields.Field10)

	a.FieldBinds = val
}

func NewApp(cfg *config.BigConfig) *App {
	app := App{}

	log.Println("in new app")
	log.Println(len(cfg.ContactFields))
	// smtpinfo := app.MakeSMTPool(cfg)

	// app.SmtpPool = smtpinfo
	app.Template = TemplateString
	app.Cfg = cfg

	app.Subject = cfg.Sending.Subject
	app.ContentType = cfg.Sending.ContentType

	app.MakeFieldMaps()
	return &app
}

func (a *App) MakeSMTPool() error {
	out := make([]SmtpInfo, 0)

	for _, v := range a.Cfg.SmtpInfo.Regs {

		smtpinfo := SmtpInfo{
			Type:       "regs",
			Host:       v.Host,
			User:       v.Username,
			Password:   v.Password,
			Port:       v.Port,
			SendAdress: v.SendAddress,
		}

		out = append(out, smtpinfo)
	}

	a.SmtpPool = out

	log.Println(a.SmtpPool)

	return nil
}

func (a *App) MainInit() error {

	log.Print("calling mainocnfig")

	log.Println(a.Cfg.BootOptions)
	switch a.Cfg.BootOptions.Main {

	case "load":
		log.Println("calling load csv")
		err := a.LoadCsv()

		if err != nil {
			log.Println("faied to create bindabales", err)
		}

		err = a.LoadTemplate()

		//log.Println(err)

		if err != nil {
			log.Println("failed to load template,  ")
			log.Println(err)
		}

		err = a.MakeSMTPool()

		if err != nil {
			log.Println("failed to make smtp pool ,  ")
			log.Println(err)
		}

		writables, err := a.MakeFinalWritables()
		if err != nil {
			log.Println("failed to make writables pool ,  ")
			log.Println(err)
		}

		log.Println("length  of writables , ", len(writables))
		//log.Println(writables)

		for k, v := range writables {
			log.Println(k)
			log.Println(v)
		}
		//err = a.SaveWritables(writables)

		err = a.SaveWritables(writables)
	}

	return nil
}

func (a *App) MakeFinalWritables() ([]FinalWritable, error) {

	out := make([]FinalWritable, 0)

	not_uniqe := make([]CSVLoaded, 0)
	not_unique_keys := make([]int, 0)
	uniquemap := make(map[string]CSVLoaded, 0)

	not_uniqe_count := 0

	for k, v := range a.Bindables {

		val, ok := uniquemap[v.Email]

		if !ok {

			uniquemap[v.Email] = v
		} else {

			if val.Email != uniquemap[val.Email].Email {
				not_uniqe = append(not_uniqe, val)
				not_unique_keys = append(not_unique_keys, k)
				not_uniqe_count++
			}
		}

	}

	log.Println("Unique Count, ", len(uniquemap))
	log.Println(not_uniqe)
	log.Println("Not Unique Count , ", not_uniqe_count)
	log.Println(not_unique_keys)

	//	smtp_limit := len(a.SmtpPool)

	smtp_random := 0
	for k, vs := range uniquemap {

		var v FinalWritable

		subj := a.GetSubject(vs)

		body := a.GetBody(vs)

		to := []string{k}

		v.ID = shortuuid.New()
		v.ContentType = a.ContentType
		v.Body = body
		v.Subject = subj
		v.To = to

		for k, vs := range a.SmtpPool {

			if k == smtp_random {
				// 	if i > smtp_random {
				smtp := vs
				v.Sender = smtp.SendAdress
				v.SmtpHost = smtp.Host
				v.SmtpPort = smtp.Port
				v.SmtpPassword = smtp.Password
				v.SmtpUser = smtp.User

				smtp_random++
				break
			} else if smtp_random > len(a.SmtpPool)-1 {
				smtp_random = 0
				smtp := vs
				v.Sender = smtp.SendAdress
				v.SmtpHost = smtp.Host
				v.SmtpPort = smtp.Port
				v.SmtpPassword = smtp.Password
				v.SmtpUser = smtp.User

				smtp_random++
				break
			}
			// } else if k > smtp_random {
			// 	// smtp := vs
			// 	// v.Sender = smtp.SendAdress
			// 	// v.SmtpHost = smtp.Host
			// 	// v.SmtpPort = smtp.Port
			// 	// v.SmtpPassword = smtp.Password
			// 	// v.SmtpUser = smtp.User
			// 	// smtp_random++
			// 	continue
			// }
		}
		out = append(out, v)
	}
	// for i := 0; i < smtp_limit; i++ {

	// 	if i > smtp_random {
	// 		smtp := a.SmtpPool[i]
	// 		v.Sender = smtp.SendAdress
	// 		v.SmtpHost = smtp.Host
	// 		v.SmtpPort = smtp.Port
	// 		v.SmtpPassword = smtp.Password
	// 		v.SmtpUser = smtp.User

	// 		smtp_random++
	// 	}else {

	// 	}

	// if smtp_limit == 1 && i == 0 {
	// 	smtp := a.SmtpPool[i]
	// 	v.Sender = smtp.SendAdress
	// 	v.SmtpHost = smtp.Host
	// 	v.SmtpPort = smtp.Port
	// 	v.SmtpPassword = smtp.Password
	// 	v.SmtpUser = smtp.User

	// 	break
	// }

	// if smtp_random > i {

	// 	smtp := a.SmtpPool[i]

	// 	v.Sender = smtp.SendAdress
	// 	v.SmtpHost = smtp.Host
	// 	v.SmtpPort = smtp.Port
	// 	v.SmtpPassword = smtp.Password
	// 	v.SmtpUser = smtp.User

	// 	smtp_random++

	// 	break

	// } else {
	// 	//smtp_random = 0
	// 	smtp := a.SmtpPool[smtp_random]
	// 	v.Sender = smtp.SendAdress
	// 	v.SmtpHost = smtp.Host
	// 	v.SmtpPort = smtp.Port
	// 	v.SmtpPassword = smtp.Password
	// 	v.SmtpUser = smtp.User

	// 	smtp_random++
	// 	break
	// }

	return out, nil

}

func (a *App) GetBody(vs CSVLoaded) string {
	fidseds := make(map[string]string)

	//log.Println(vs)

	//log.Println(a.FieldBinds)

	for k, v := range a.FieldBinds {

		//log.Println(k)
		//log.Println(v)

		for _, vv := range vs.Fields {

			//log.Println(vv.FieldName)
			//log.Println(vv.FieldName)
			if vv.FieldName == v {

				fidseds[k] = vv.Value

				//log.Println(vv.FieldName)
				//log.Println(vv.Value)

				break
			}
		}
	}

	//t, err := template.New("todos").Parse("You have a task named \"{{ .Name}}\" with description: \"{{ .Description}}\"")
	t, err := template.New("body").Parse(a.Template)
	if err != nil {
		log.Println("failed to parse")
		log.Println(err)
		//panic(err)
	}
	buf := new(bytes.Buffer)
	//f(buf)

	//err = t.E
	err = t.Execute(buf, fidseds)
	if err != nil {
		panic(err)
	}

	//log.Println(buf.String())

	return buf.String()
}

func (a *App) GetSubject(vs CSVLoaded) string {

	fidseds := make(map[string]string)

	//log.Println(vs)

	//log.Println(a.FieldBinds)

	for k, v := range a.FieldBinds {

		//log.Println(k)
		//log.Println(v)

		for _, vv := range vs.Fields {

			//log.Println(vv.FieldName)
			//log.Println(vv.FieldName)
			if vv.FieldName == v {

				fidseds[k] = vv.Value

				//log.Println(vv.FieldName)
				//log.Println(vv.Value)

				break
			}
		}

		//name fined name

		// val, ok := a.KeysContact[v]

		// if !ok {
		// 	log.Println("value does not exist")
		// }

	}

	//t, err := template.New("todos").Parse("You have a task named \"{{ .Name}}\" with description: \"{{ .Description}}\"")
	t, err := template.New("subject").Parse(a.Subject)
	if err != nil {
		log.Println("failed to parse")
		log.Println(err)
		//panic(err)
	}
	buf := new(bytes.Buffer)
	//f(buf)

	//err = t.E
	err = t.Execute(buf, fidseds)
	if err != nil {
		panic(err)
	}
	//log.Println("buff called")
	//log.Println(a.Subject)
	//log.Println(buf.String())

	return buf.String()

}

func (a *App) SaveWritables(as []FinalWritable) error {

	data := as
	map_json := make(map[string]interface{})
	map_json["data"] = data
	map_json["pop"] = 600
	//file, err := json.MarshalIndent(map_json, "", " ")

	file, err := json.Marshal(map_json)

	if err != nil {

		log.Println(err)

		return err
	}
	now := time.Now()

	writable_name := fmt.Sprint("writable_for", now.Day(), now.Month(), now.Minute(), now.Second(), ".json")

	f, _ := os.Executable()

	dir := filepath.Dir(f)

	path_to_csv := filepath.Join(dir, "data", "reports")
	log.Println("getting sending list")
	log.Println(a.Cfg.Sending)
	file_to_save := filepath.Join(path_to_csv, writable_name)

	err = ioutil.WriteFile(file_to_save, file, 0644)

	if err != nil {

		log.Println("failed to save file")
		log.Println(err)
	}

	return err
}
func (a *App) Main() {

	a.MainInit()
	//td := Todo{"Test templates", "Let's test a template to see the magic."}

	// out := make([]FinalWritable, 0)

	// td := make(map[string]string, 0)

	// td["field1"] = "Testing name here"
	// td["field2"] = "1065885"
	// td["field3"] = "3257788"
	// td["field4"] = "columbus"
	// td["field5"] = "OH"

	// //t, err := template.New("todos").Parse("You have a task named \"{{ .Name}}\" with description: \"{{ .Description}}\"")
	// t, err := template.New("todos").Parse(a.Template)
	// if err != nil {
	// 	log.Println("failed to parse")
	// 	log.Println(err)
	// 	//panic(err)
	// }
	// buf := new(bytes.Buffer)
	// //f(buf)

	// //err = t.E
	// err = t.Execute(buf, td)
	// if err != nil {
	// 	panic(err)
	// }

	// log.Println(t.DefinedTemplates())

	// var wr FinalWritable

	// wr.Subject = "some Subject"
	// wr.Template = buf.String()
	// wr.ToAddress = "kobysam13@gmail.com"
	// wr.ReplyTo = "koby@pcarriers"
	// wr.SmtpInfo = SmtpInfo{Type: "reg"}

	// out = append(out, wr)

	// var FW ListOfWritable

	// FW = out

	// FW.PrintAll()
}

func (a *App) ConvertToNew(fields map[string]string) string {

	return string("s")
}

func (a *App) BootUP() {

}
