package digestapp

import (
	"github.com/go-chi/chi"
	"gorm.io/gorm"
)

type DBExecutor struct {
	DB *gorm.DB
}

type StationServerMsg struct {
	Msg      int
	Value    interface{}
	Executor *DBExecutor
}

type DigestApp struct {
	Station   *ApplicationStation
	Executor  *DBExecutor
	R         chi.Router
	CommsChan chan StationServerMsg
	Port      string
}

func (da *DigestApp) Init() error {
	return nil
}

func (da *DigestApp) BuildRoutes() error {

	r := chi.NewRouter()

	r.Post("/consume", da.ConsumeHandler)
	r.Get("/report", da.ReportHandler)
	r.Get("/loaded", da.LoadedHandler)
	r.Post("/rebase", da.RebaseHandler)
	r.Get("/activitydetail", da.GetActivityDetail)
	r.Post("/loadone", da.LoadOneHander)
	return nil
}

func (da *DigestApp) AsyncStart() {

}
