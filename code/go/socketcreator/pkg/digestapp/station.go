package digestapp

import (
	"encoding/csv"
	"log"
	"os"
	"path/filepath"
	"sync"

	"github.com/lithammer/shortuuid"
)

type ApplicationStation struct {
	BasePath    string
	Consuming   bool
	CanConsume  bool
	Ex          *DBExecutor
	CurrentPool []Digest
	mu          sync.Mutex
	LastIndex   int
	CurrentID   string
	Wg          sync.WaitGroup
}

func (as *ApplicationStation) HandleDigestCreation(data LoadOneRequest, cpid string) ([]Digest, error) {

	out := make([]Digest, 0)

	path_f := filepath.Join(as.BasePath, "data", "indata", data.CsvName)

	f, _ := os.Open(path_f)

	csvReader := csv.NewReader(f)
	data_csv, err := csvReader.ReadAll()
	if err != nil {
		log.Println(err)
	}

	for k, v := range data_csv {

		if k > 0 && len(v) > 1 {

			d := NewDigest(cpid)

			d.Name = v[0]

			out = append(out, *d)

		}

	}

	return out, nil

}

func (as *ApplicationStation) HandleData(datain LoadOneRequest) {

	campaign := Campaign{
		ID:   shortuuid.New(),
		Name: datain.CampaignName,
	}

	//remember to break if campaign failes
	err := as.Ex.CreateCampaign(campaign)

	if err != nil {
		log.Println("failed to create campaign")
		log.Println(err)
	}

	data, err := as.HandleDigestCreation(datain, campaign.ID)

	if err != nil {

		log.Println("failed to create")
	}

	err = as.Ex.CreateDigest(data)

	if err != nil {
		log.Println("failed to create digest")
	}

	//close wg here

}
