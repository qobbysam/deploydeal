package digestapp

import (
	"log"
	"time"

	"github.com/lithammer/shortuuid"
	"github.com/qobbysam/socketcreator/pkg/config"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

type Campaign struct {
	ID   string `gorm:"primaryKey"`
	Name string
}

type PreRunInfo struct {
	ID string `gorm:"primaryKey"`
}

type SomeRunInfo struct {
	ID string `gorm:"primaryKey"`
}

type Content struct {
	ID string `gorm:"primaryKey"`
}

type Digest struct {
	ID            string `gorm:"primaryKey"`
	Name          string
	Created       time.Time
	LastUpdated   time.Time
	Modified      bool
	CampaignID    string
	PreRunInfoID  string
	SomeRunInfoID string
	ContentID     string
}

func NewDBExecutor(cfg *config.BigConfig) (*DBExecutor, error) {

	name := "local"

	db, err := gorm.Open(sqlite.Open(name), &gorm.Config{})

	if err != nil {
		log.Println("failed to print errors")
		return nil, err
	}

	ex := DBExecutor{
		DB: db,
	}

	ex.RunMigrations()

	return &ex, nil
}

//work on better creation of digest
func NewDigest(id string) *Digest {

	return &Digest{
		ID:         shortuuid.New(),
		CampaignID: id,
		Created:    time.Now(),
		Modified:   false,
	}
}
func (de *DBExecutor) RunMigrations() {

	de.DB.AutoMigrate(&Digest{}, &Content{}, &SomeRunInfo{}, &PreRunInfo{}, &Campaign{})

}

func (de *DBExecutor) AllCampaigns() ([]Campaign, error) {

	out := make([]Campaign, 0)

	err := de.DB.Find(&out)

	if err.Error != nil {
		return nil, err.Error
	}

	return out, nil
}

func (de *DBExecutor) ACampaignDigest(id string) ([]Digest, error) {
	out := make([]Digest, 0)

	err := de.DB.Where(&Digest{CampaignID: id}).Find(&out)

	if err.Error != nil {
		return nil, err.Error
	}

	return out, nil
}

func (de *DBExecutor) NotModifiedCampaignDigest(id string) ([]Digest, error) {
	out := make([]Digest, 0)

	err := de.DB.Where(&Digest{CampaignID: id, Modified: false}).Find(&out)

	if err.Error != nil {
		return nil, err.Error
	}

	return out, nil
}

func (de *DBExecutor) ModifiedCampaignDigest(id string) ([]Digest, error) {
	out := make([]Digest, 0)

	err := de.DB.Where(&Digest{CampaignID: id, Modified: true}).Find(&out)

	if err.Error != nil {
		return nil, err.Error
	}

	return out, nil
}

func (de *DBExecutor) CreateCampaign(ca Campaign) error {

	ex := de.DB.Create(ca)

	return ex.Error
}

func (de *DBExecutor) CreateDigest(ca []Digest) error {

	ex := de.DB.CreateInBatches(ca, 500)

	return ex.Error
}
