package executor

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"path/filepath"
	"sync"
	"time"
)

type Executable struct {
	ID           string   `json:"id"`
	Sender       string   `json:"sender"`
	To           []string `json:"tolist"`
	SmtpUser     string   `json:"smtpuser"`
	SmtpPassword string   `json:"smtppassword"`
	SmtpPort     string   `json:"smtpport"`
	SmtpHost     string   `json:"smtphost"`
	Subject      string   `json:"subject"`
	Body         string   `json:"body"`
	ContentType  string   `json:"contenttype"`
}

type Report struct {
	ID     string
	Action string
}

type LoadedExecutables struct {
	Pop    int
	BLimit int
	SLimit int
	Data   []Executable
}

type Run struct {
	Current []Executable
	LastKey int
}

func (a *Executable) SendGenMsg() error {

	log.Println("sendmsg has been called successfully")

	// m := gomail.NewMessage()

	// // Set E-Mail sender
	// m.SetHeader("From", a.Sender)

	// // Set E-Mail receivers
	// m.SetHeader("To", a.To...)

	// // Set E-Mail subject
	// m.SetHeader("Subject", a.Subject)

	// // Set E-Mail body. You can set plain text or html with text/html
	// m.SetBody(a.ContentType, a.Body)

	// // Settings for SMTP server
	// port, _ := strconv.Atoi(a.SmtpPort)
	// d := gomail.NewDialer(a.SmtpHost, port, a.Sender, a.SmtpPassword)
	// //port, _ := strconv.Atoi(em.SmtpAuth.Port)
	// //d := gomail.NewDialer(em.SmtpAuth.Host, port, em.SmtpAuth.From, em.SmtpAuth.Password)

	// // This is only needed when SSL/TLS certificate is not valid on server.
	// // In production this should be set to false.
	// d.TLSConfig = &tls.Config{InsecureSkipVerify: true}

	// // Now send E-Mail
	// if err := d.DialAndSend(m); err != nil {
	// 	fmt.Println(err)
	// 	//panic(err)
	// }

	return nil
	//return nil
}

func (ex *Executable) HandleExecution(report chan interface{}, when time.Duration) {

	time.Sleep(when)

	err := ex.SendGenMsg()

	if err != nil {

		reportmsg := "run failed" + ex.ID

		report <- reportmsg
	} else {

		reportmsg := "run passed " + ex.ID
		report <- reportmsg
	}
}

type ExApp struct {
	Pop            int
	CanContinue    bool
	All            []Executable
	ReportChan     chan interface{}
	ReceivedReport int
	Reports        []interface{}
	LastKey        int
	WG             sync.WaitGroup
	TotalReceived  int
	CycleReceived  int
	Complete       bool
}

type BulkUp struct {
	Data []Executable `json:"data"`
	Pop  int          `json:"pop"`
}

func (ex *ExApp) DigestAll(name string) error {

	ospath, _ := os.Executable()

	base := filepath.Dir(ospath)

	//name := "try1.json"

	to_use := filepath.Join(base, "data", "exdata", name)

	f, err := os.Open(to_use)

	if err != nil {
		log.Println("failed to open all files")

	}

	by, err := ioutil.ReadAll(f)

	if err != nil {
		fmt.Println("failed read file ,  ", err)

	}

	var im BulkUp

	err = json.Unmarshal(by, &im)

	if err != nil {
		fmt.Println("failed to marshal json check order,  ", err)

	}

	if im.Pop == 0 {
		ex.Pop = 600

	} else {
		ex.Pop = im.Pop
	}

	ex.All = im.Data
	//	ex.Pop = im.Pop
	ex.CanContinue = true
	ex.ReportChan = make(chan interface{}, len(ex.All))
	ex.WG = sync.WaitGroup{}
	ex.LastKey = 0
	ex.Reports = make([]interface{}, 0)
	ex.TotalReceived = 0
	ex.CycleReceived = 0
	ex.Complete = false
	return nil

}

func (ex *ExApp) BuildReport() error {

	data := ex.Reports
	map_json := make(map[string]interface{})
	map_json["data"] = data
	map_json["pop"] = 600
	//file, err := json.MarshalIndent(map_json, "", " ")

	file, err := json.Marshal(map_json)

	if err != nil {

		log.Println(err)

		return err
	}
	now := time.Now()

	writable_name := fmt.Sprint("reports_at", now.Day(), now.Month(), now.Minute(), now.Second(), ".json")

	f, _ := os.Executable()

	dir := filepath.Dir(f)

	path_to_json := filepath.Join(dir, "data", "exreport")
	//log.Println("getting sending list")
	//log.Println(a.Cfg.Sending)
	file_to_save := filepath.Join(path_to_json, writable_name)

	err = ioutil.WriteFile(file_to_save, file, 0644)

	if err != nil {

		log.Println("failed to save file")
		log.Println(err)
	}

	return err
}

func (ex *ExApp) Shutdown() error {
	err := ex.BuildReport()

	return err
}

func (ex *ExApp) StartConsuming() {

	total := len(ex.All)

	min_run := total / ex.Pop

	//	report_chan := make(chan interface{}, ex.Pop)

	log.Println("starting wathers")
	ex.WG.Add(1)
	go ex.WatchReports()
	i := 0

	log.Println("finished watches")

	log.Println(min_run)
	if min_run <= 0 {
		log.Println("Starting Next Process", time.Now())

		top_process := ex.PopNext()

		execute_time := ex.NextTime()
		for k, v := range top_process {
			ex.WG.Add(1)
			go v.HandleExecution(ex.ReportChan, execute_time[k])

		}
		ex.WG.Wait()
		return
	}

	for i < min_run {

		if !ex.CanContinue {
			continue

		} else {

			log.Println("Starting Next Process", time.Now())

			top_process := ex.PopNext()

			execute_time := ex.NextTime()

			log.Println("len of tp  ", len(top_process))

			for k, v := range top_process {
				ex.WG.Add(1)
				log.Println(v)
				go v.HandleExecution(ex.ReportChan, execute_time[k])

			}
			ex.CanContinue = false
			i++
		}

		if ex.Complete {
			break

		}
	}
	ex.WG.Wait()

	log.Println("This cycle is complete")

}

func (ex *ExApp) WatchReports() {

	for {
		select {
		case v := <-ex.ReportChan:

			ex.TotalReceived++
			//ex.CycleReceived++

			if ex.ReceivedReport < ex.Pop {
				ex.Reports = append(ex.Reports, v)
				ex.ReceivedReport++

				ex.WG.Done()

			} else {

				//if time is not enough

				//sleep some more before

				ex.Reports = append(ex.Reports, v)
				ex.CanContinue = true
				ex.ReceivedReport = 0

				//save current reports

				//reset reports to zero
			}
			//ex.ReceivedReport ++

			if ex.TotalReceived == len(ex.All) {

				log.Println("all have been processed")

				_ = ex.Shutdown()

				ex.WG.Done()
				ex.Complete = true
			}

		}
	}

}

func (ex *ExApp) PopNext() []Executable {

	//last := ex.LastKey

	limit := len(ex.All)

	topop := ex.Pop
	counter := 0

	current := make([]Executable, 0)

	min_run := limit / topop

	if min_run <= 0 {

		current = append(current, ex.All...)

		return current
	}

	for k, v := range ex.All {

		if k >= ex.LastKey && counter < ex.Pop {
			current = append(current, v)
			counter++
		} else {
			ex.LastKey = k
			counter = 0
			break
		}
	}

	// for i := 0; i < limit; i++ {

	// 	if i > last && i < topop {
	// 		//pop exact needed
	// 		if counter < topop {

	// 			current = append(current, ex.All[i])

	// 			counter++

	// 		} else {
	// 			counter = 0
	// 			ex.LastKey = i
	// 			break
	// 		}

	// 	} else {
	// 		//add remaining after pop

	// 		current = ex.All[ex.LastKey:]
	// 	}

	// }

	return current

}

func (ex *ExApp) NextTime() []time.Duration {

	//now := time.Now()

	//next_10 := now.Add(10 * time.Second)

	min := len(ex.All) / ex.Pop
	limit := 60 * time.Second

	total_ := time.Duration(ex.Pop) * time.Second

	perminute := total_ / limit

	localperminute := 0
	coscent := limit

	if min < 0 {

		out := make([]time.Duration, len(ex.All))

		for k, _ := range out {

			out[k] = time.Duration(coscent) + RandMillisecond()
		}

		return out
	}

	out := make([]time.Duration, ex.Pop)

	for i := 0; i < ex.Pop; i++ {

		if localperminute < int(perminute) {

			out[i] = time.Duration(coscent) + RandMillisecond()
			localperminute++

		} else {
			out[i] = time.Duration(coscent) + RandMillisecond()
			coscent = coscent + limit
			localperminute = 0

		}
	}

	// for k, _ := range out {

	// 	if localperminute < perminute {

	// 		//out[k] = time.Duration(coscent*time.Second) + RandMillisecond()
	// 		//localperminute++
	// 	} else {
	// 		coscent = coscent + limit
	// 		out[k] = time.Duration(coscent) + RandMillisecond()
	// 		localperminute = 0

	// 	}
	// }

	log.Println(out)

	return out
}

func RandMillisecond() time.Duration {
	rand.Seed(time.Now().UnixNano())
	min := 240
	max := 350
	n := rand.Intn(max-min+1) + min
	//n := rand.Intn(30)
	return time.Duration(n * int(time.Millisecond))
}

func NewExecApp(name string) *ExApp {
	app := ExApp{}

	err := app.DigestAll(name)

	if err != nil {
		log.Println(err)
	}

	return &app
}
