package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

type BootOptions struct {
	Main string `json:"main"`
}

type SendingConfig struct {
	Subject      string `json:"subject"`
	ListName     string `json:"listname"`
	TemplateName string `json:"templatename"`
	ContentType  string `json:"contenttype"`
}

type TemplateFields struct {
	Field1  string `json:"field1"`
	Field2  string `json:"field2"`
	Field3  string `json:"field3"`
	Field4  string `json:"field4"`
	Field5  string `json:"field5"`
	Field6  string `json:"field6"`
	Field7  string `json:"field7"`
	Field8  string `json:"field8"`
	Field9  string `json:"field9"`
	Field10 string `json:"field10"`
}

type YahooSmtpInfo struct {
	Host        string `json:"host"`
	Username    string `json:"username"`
	Password    string `json:"password"`
	Port        string `json:"port"`
	SendAddress string `json:"sendaddress"`
}

type RegInfo struct {
	Host        string `json:"host"`
	Username    string `json:"username"`
	Password    string `json:"password"`
	Port        string `json:"port"`
	SendAddress string `json:"sendaddress"`
}

type SmtpInfo struct {
	Yahoo []YahooSmtpInfo `json:"yahoo"`
	Regs  []RegInfo       `json:"regs"`
}

type BigConfig struct {
	BootOptions    *BootOptions    `json:"bootoptions"`
	Sending        *SendingConfig  `json:"sending"`
	ContactFields  []string        `json:"contactfields"`
	TemplateFields *TemplateFields `json:"templatefields"`
	SmtpInfo       *SmtpInfo       `json:"smtpinfo"`
}

func NewBigConfig(path string) (*BigConfig, error) {

	f, err := os.Open(path)

	if err != nil {
		log.Println("failed to open all files")

		return nil, err
	}

	by, err := ioutil.ReadAll(f)

	if err != nil {
		fmt.Println("failed read file ,  ", err)
		return nil, err
	}

	var bigConfig BigConfig

	err = json.Unmarshal(by, &bigConfig)

	if err != nil {
		fmt.Println("failed to marshal json check order,  ", err)
		return nil, err
	}

	return &bigConfig, nil
}
