package internal

import (
	"log"
	"os"
	"path/filepath"

	"github.com/qobbysam/socketcreator/pkg/app"
	"github.com/qobbysam/socketcreator/pkg/config"
)

type InternalStruct struct {
	App *app.App
}

func (a *InternalStruct) Main() {

	a.App.Main()
}

func NewInternalStruct(path string) *InternalStruct {

	ospath, _ := os.Executable()

	base := filepath.Dir(ospath)

	to_use := ""

	if path == "" {
		to_use = filepath.Join(base, "data", "conf.json")
	} else {
		to_use = path
	}
	config, err := config.NewBigConfig(to_use)

	if err != nil {
		log.Println("failed to create config")
		log.Panic(err)
	}

	log.Println(config)

	app := app.NewApp(config)

	return &InternalStruct{
		App: app,
	}
}
