package internal

import (
	"log"

	"github.com/qobbysam/socketcreator/pkg/executor"
)

type ExecInternal struct {
	Name string
}

func NewExecInternal(name string) *ExecInternal {

	return &ExecInternal{Name: name}
}

func (ex *ExecInternal) Main() {

	app := executor.NewExecApp(ex.Name)

	log.Println(app.All)
	log.Println("lenght of all ", len(app.All))

	app.StartConsuming()

}
