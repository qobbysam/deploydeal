module github.com/qobbysam/socketcreator

go 1.17

require (
	github.com/go-chi/chi v1.5.4
	github.com/lithammer/shortuuid v3.0.0+incompatible
	github.com/qobbysam/socketnotify v0.0.0-20220330192813-52e7dae3c83b
	gopkg.in/mail.v2 v2.3.1
	gorm.io/driver/sqlite v1.3.1
	gorm.io/gorm v1.23.4
)

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.4 // indirect
	github.com/mattn/go-sqlite3 v1.14.9 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
)
