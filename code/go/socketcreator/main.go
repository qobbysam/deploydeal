package main

import (
	"flag"

	"github.com/qobbysam/socketcreator/internal"
)

func main() {

	cpath := flag.String("c", "", "path to config file")
	flag.Parse()
	is := internal.NewInternalStruct(*cpath)

	is.Main()
}
