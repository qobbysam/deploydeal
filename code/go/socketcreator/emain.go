package main

import (
	"flag"

	"github.com/qobbysam/socketcreator/internal"
)

func main() {

	name := flag.String("n", "", "name of data json file")

	flag.Parse()

	ex := internal.NewExecInternal(*name)

	ex.Main()

}
