package app

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"path/filepath"
	"strings"

	"github.com/go-chi/chi"
	"github.com/mholt/binding"
	"github.com/qobbysam/collectbasic/pkgs/config"
	"github.com/qobbysam/collectbasic/pkgs/locdb"
)

type AppServer struct {
	Router         chi.Router
	DB             *locdb.DBEX
	TP             *TemplateManager
	CreateUrl      string
	CreatePostUrl  string
	ObserveUrl     string
	ObservePostUrl string
	ObserveOneUrl  string
	CollectUrl     string
	ClientPostUrl  string
	Authstring     string
	Port           string
	AssetsDir      string
	AppUrl         string
}

func NewAppServer(cfg *config.BigConfig) *AppServer {
	funcMap := template.FuncMap{
		"customMethod": func(thing string) string {
			return fmt.Sprintf("Custom Method in funcMap added in main.go %s", thing)
		},
	}

	db, err := locdb.NewDBEX(cfg)

	if err != nil {
		log.Println("failed to create Data base")
	}

	dir := filepath.Join(cfg.DataLoc, "data", "static")

	layout := filepath.Join(dir, "layouts")
	exec_str := ".gohtml"
	//exec_fun := viewHelpers

	develev := true

	log.Println(dir)
	log.Println(layout)
	tp, err := NewTemplateManager(dir, layout, exec_str, funcMap, develev)
	if err != nil {
		log.Println("failed to Build Template builder")
	}
	assets_dir := filepath.Join(cfg.DataLoc, "data", "assets")
	out := AppServer{
		CreateUrl:     cfg.BootOpt.CreateUrl,
		CreatePostUrl: cfg.BootOpt.CreatePostUrl,
		ObserveUrl:    cfg.BootOpt.ObserveUrl,

		CollectUrl:     cfg.BootOpt.CollectUrl,
		ClientPostUrl:  cfg.BootOpt.ClientPostUrl,
		ObserveOneUrl:  cfg.BootOpt.ObserveOneUrl,
		ObservePostUrl: cfg.BootOpt.ObservePostUrl,
		Port:           cfg.BootOpt.Port,
		AssetsDir:      assets_dir,
		AppUrl:         cfg.BootOpt.AppUrl,
	}

	out.DB = db
	out.TP = tp
	return &out
}

func (app *AppServer) BuildRoutes() {

	r := chi.NewRouter()
	r.Get("/", app.DefaultHandler)
	//
	r.Get(app.CreateUrl, app.CreateGetHandler)
	r.Post(app.CreateUrl, app.CreatePostHandler)
	r.Get(app.ObserveUrl, app.ObserveGetUrlHandler)
	r.Get(app.ObserveOneUrl, app.ObserveOneHandler)
	//r.Post(app.ObserveOneUrl, app.ObserveOnePostHander)
	r.Post(app.ObservePostUrl, app.ObservePostUrlHandler)

	r.Get(app.CollectUrl, app.ClientGetHandler)
	r.Post(app.ClientPostUrl, app.ClientPostHandler)

	app.Router = r
}

func (app *AppServer) DefaultHandler(w http.ResponseWriter, r *http.Request) {
	//renderPage :=
	app.RenderPage("/default", "", w, r)
}

func (app *AppServer) CreateGetHandler(w http.ResponseWriter, r *http.Request) {

	allcreate, err := app.DB.GetAllCreate()

	if err != nil {
		//handle the error
		log.Println("DB Get failed")
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	out_data := make(map[string]interface{})

	outlist := make([]CreateMsg, 0)

	for _, v := range allcreate {
		val := CreateMsg{}
		val.ID = v.ID
		val.MC = v.MC
		val.ClientID = v.ClientID
		val.ObserverID = v.WorkerObserverID
		val.Email = v.Email
		outlist = append(outlist, val)
	}
	out_data["createlist"] = outlist

	app.RenderPage(app.CreateUrl, out_data, w, r)
}

// func (app *AppServer) CreateUrlHandler(w http.ResponseWriter, r *http.Request) {

// }

func (app *AppServer) CreatePostHandler(w http.ResponseWriter, r *http.Request) {
	data := new(CreatePostRequest)
	if errs := binding.Bind(r, data); errs != nil {
		log.Println("bind faileds")
		http.Error(w, errs.Error(), http.StatusBadRequest)
		return
	}

	client, err := app.DB.CreateClientSubmission(data.Mc, data.Email, data.WorkerObserverID)

	if err != nil {
		//handle err
		log.Println("Error saving failed")
		http.Error(w, err.Error(), http.StatusBadRequest)

	}
	out_data := make(map[string]interface{})
	msgs, err := app.DB.GetAllCreate()

	if err != nil {
		fmt.Println("err ", err)
		log.Println("DB msg failed failed")
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	msglist := make([]CreateMsg, 0)

	for _, v := range msgs {
		val := CreateMsg{}
		val.ID = v.ID
		val.MC = v.MC
		val.ClientID = v.ClientID
		val.ObserverID = v.WorkerObserverID
		val.Email = v.Email
		msglist = append(msglist, val)
	}

	//out_data["observelist"] = msglist
	out_data["messages"] = client.ID + " " + client.Email + "  " + client.MC + " resceived successfully"
	out_data["createlist"] = msglist
	log.Println(out_data, "in handler")
	app.RenderPage(app.CreateUrl, out_data, w, r)

}

//func (app *AppServer) BuildGetData()

func (app *AppServer) ObserveGetUrlHandler(w http.ResponseWriter, r *http.Request) {

	data, err := app.DB.GetAllObservers()
	if err != nil {

		log.Println("DB Get failed")
		http.Error(w, err.Error(), http.StatusBadRequest)
		return

	}
	out_data := make(map[string]interface{})

	outlist := make([]ObserveMsg, 0)

	for _, v := range data {
		val := ObserveMsg{}
		val.ID = v.ID
		val.Name = v.Name
		outlist = append(outlist, val)
	}
	out_data["observelist"] = outlist

	//out_data["observelist"] = app()
	app.RenderPage(app.ObserveUrl, out_data, w, r)
}

func (app *AppServer) ObserveOneHandler(w http.ResponseWriter, r *http.Request) {
	key := r.URL.Query().Get("id")

	if key == "" {
		//render.Render(rw, r, ErrInvalidRequest(errors.New("key cannot be null")))
		app.DefaultHandler(w, r)
		return
	}

	//clientinfo, err := app.DB.GetClientInfo(key)

	// if err != nil {
	// 	//render.Render(rw, r, ErrInvalidRequest(errors.New("key cannot be null")))
	// 	app.DefaultHandler(w, r)
	// 	return
	// }

	// if !clientinfo.Alive {
	// 	app.DefaultHandler(w, r)
	// 	return
	// }

	all_subs, err := app.DB.BuildObserveOneData(key)
	if err != nil {
		//render.Render(rw, r, ErrInvalidRequest(errors.New("key cannot be null")))
		app.DefaultHandler(w, r)
		return
	}
	msglist := make([]ObserveResponseMsg, 0)
	out_data := make(map[string]interface{})

	for _, v := range all_subs {
		ap := ObserveResponseMsg{}
		//	ap.ID = v.ID
		//	ap.Name = v.Name
		ap.ID = fmt.Sprint(v.ID)
		ap.Value = v.Value
		ap.CreatedTime = v.CreatedTime
		ap.ClientObserverID = v.ClientObserverID
		ap.ClientID = v.ClientID
		msglist = append(msglist, ap)
	}

	//out_data["observelist"] = msglist
	//out_data["messages"] = obj.ID + " " + data.Name + " resceived successfully"
	out_data["observeonelist"] = msglist
	log.Println(out_data, "in handler")

	app.RenderPage(app.ObserveOneUrl, out_data, w, r)
	//app.RenderPage(app.ObserveOneUrl, "", w, r)
}
func (app *AppServer) ObserveOnePostHander(w http.ResponseWriter, r *http.Request) {

	app.RenderPage(app.ObserveOneUrl, "", w, r)
}
func (app *AppServer) ObservePostUrlHandler(w http.ResponseWriter, r *http.Request) {

	data := new(ObservePostRequest)
	if errs := binding.Bind(r, data); errs != nil {
		log.Println("bind failed")
		http.Error(w, errs.Error(), http.StatusBadRequest)
		return
	}

	obj, err := app.DB.CreateWorkerObserver(data.Name)

	if err != nil {
		log.Println("Save DB faild failed")
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	out_data := make(map[string]interface{})
	msgs, err := app.DB.GetAllObservers()

	if err != nil {
		fmt.Println("err ", err)
		log.Println("DB msg failed failed")
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	msglist := make([]ObserveMsg, 0)

	for _, v := range msgs {
		ap := ObserveMsg{}
		ap.ID = v.ID
		ap.Name = v.Name
		msglist = append(msglist, ap)
	}

	//out_data["observelist"] = msglist
	out_data["messages"] = obj.ID + " " + data.Name + " resceived successfully"
	out_data["observelist"] = msglist
	log.Println(out_data, "in handler")
	app.RenderPage(app.ObservePostUrl, out_data, w, r)

}

func (app *AppServer) ClientGetHandler(w http.ResponseWriter, r *http.Request) {

	key := r.URL.Query().Get("id")

	if key == "" {
		//render.Render(rw, r, ErrInvalidRequest(errors.New("key cannot be null")))
		app.DefaultHandler(w, r)
		return
	}

	clientinfo, err := app.DB.GetClientInfo(key)

	if err != nil {
		//render.Render(rw, r, ErrInvalidRequest(errors.New("key cannot be null")))
		app.DefaultHandler(w, r)
		return
	}

	if !clientinfo.Alive {
		app.DefaultHandler(w, r)
		return
	}

	all_subs, err := app.DB.BuildClientData(clientinfo.ClientID)
	if err != nil {
		//render.Render(rw, r, ErrInvalidRequest(errors.New("key cannot be null")))
		app.DefaultHandler(w, r)
		return
	}
	msglist := make([]ClientResponseMsg, 0)
	out_data := make(map[string]interface{})

	for _, v := range all_subs {
		ap := ClientResponseMsg{}
		//	ap.ID = v.ID
		//	ap.Name = v.Name
		ap.Time = fmt.Sprint(v.CreatedTime)
		ap.Msg = "ok"
		msglist = append(msglist, ap)
	}

	//out_data["observelist"] = msglist
	//out_data["messages"] = obj.ID + " " + data.Name + " resceived successfully"
	out_data["clientlist"] = msglist
	log.Println(out_data, "in handler")

	app.RenderPage(app.CollectUrl, out_data, w, r)
}

func (app *AppServer) ClientPostHandler(w http.ResponseWriter, r *http.Request) {
	key := r.URL.Query().Get("id")

	if key == "" {
		//render.Render(rw, r, ErrInvalidRequest(errors.New("key cannot be null")))
		app.DefaultHandler(w, r)
		return
	}

	clientinfo, err := app.DB.GetClientInfo(key)

	if err != nil {
		//render.Render(rw, r, ErrInvalidRequest(errors.New("key cannot be null")))
		app.DefaultHandler(w, r)
		return
	}

	if !clientinfo.Alive {
		app.DefaultHandler(w, r)
		return
	}

	data := new(ClientPostRequest)
	if errs := binding.Bind(r, data); errs != nil {
		log.Println("bind failed")
		http.Error(w, errs.Error(), http.StatusBadRequest)
		return
	}

	obj, err := app.DB.CreateClientCollect(data.Value, clientinfo.WorkerObserverID, clientinfo.ClientID)

	if err != nil {
		log.Println("Save DB faild failed")
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	all_subs, err := app.DB.BuildClientData(clientinfo.ClientID)
	if err != nil {
		//render.Render(rw, r, ErrInvalidRequest(errors.New("key cannot be null")))
		app.DefaultHandler(w, r)
		return
	}
	msglist := make([]ClientResponseMsg, 0)
	out_data := make(map[string]interface{})

	for _, v := range all_subs {
		ap := ClientResponseMsg{}
		//	ap.ID = v.ID
		//	ap.Name = v.Name
		ap.Time = fmt.Sprint(v.CreatedTime)
		ap.Msg = "ok"
		msglist = append(msglist, ap)
	}

	//out_data["observelist"] = msglist
	out_data["messages"] = fmt.Sprint(obj.CreatedTime, "   resceived successfully")
	out_data["clientlist"] = msglist
	log.Println(out_data, "in handler")

	app.RenderPage(app.CollectUrl, out_data, w, r)
}

func (app *AppServer) Init() error {

	app.BuildRoutes()
	return nil
}

func (app *AppServer) StartServer() error {

	fmt.Println("Started Successfull")
	log.Println(app)
	err := app.Init()

	if err != nil {
		fmt.Println("failed to init")
	}

	log.Println(app)
	fmt.Println("hitting http listen and serve,  ", app.Port)
	//root := filepath.Dir(app.AssetsDir)
	root := app.AssetsDir

	fileServer := http.FileServer(http.Dir(root))
	//http.Handle("/assets/*", http.StripPrefix("/assets", fileServer))
	log.Println(root)
	log.Println(fileServer)
	//	log.Println(http.StripPrefix("/assets", fileServer))

	app.Router.Handle("/assets/*", http.StripPrefix("/assets", fileServer))
	//router.Handle("/", app.Router)
	log.Println("hiting serve")

	walkFunc := func(method string, route string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
		route = strings.Replace(route, "/*/", "/", -1)
		fmt.Printf("%s %s\n", method, route)
		return nil
	}

	if err := chi.Walk(app.Router, walkFunc); err != nil {
		fmt.Printf("Logging err: %s\n", err.Error())
	}
	err = http.ListenAndServe(app.Port, app.Router)

	return err

}
