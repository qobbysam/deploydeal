package app

import (
	"fmt"
	"html/template"
	"log"
	"net/http"

	"github.com/go-chi/render"
	"github.com/gorilla/sessions"
	"github.com/mholt/binding"
)

type CreatePostRequest struct {
	Mc               string `json:"mc"`
	Email            string `json:"email"`
	WorkerObserverID string `json:"observerid"`
}

func (c *CreatePostRequest) Bind() error {
	return nil
}

func (cf *CreatePostRequest) FieldMap(req *http.Request) binding.FieldMap {
	return binding.FieldMap{
		&cf.Mc:               "mc",
		&cf.Email:            "email",
		&cf.WorkerObserverID: "observerid",

		// &cf.Email:   "email",
		// &cf.Message: binding.Field{
		// 	Form:     "message",
		// 	Required: true,
		// },
	}
}

type ClientPostRequest struct {
	Value string `json:"value"`
}

func (cf *ClientPostRequest) FieldMap(req *http.Request) binding.FieldMap {
	return binding.FieldMap{
		&cf.Value: "value",
		//&cf.Email:            "email",
		//&cf.WorkerObserverID: "observerid",

		// &cf.Email:   "email",
		// &cf.Message: binding.Field{
		// 	Form:     "message",
		// 	Required: true,
		// },
	}
}
func (c *ClientPostRequest) Bind() error {
	return nil
}

type ObservePostRequest struct {
	Name string `json:"name"`
}

func (c *ObservePostRequest) Bind(r *http.Request) error {
	return nil
}
func (cf *ObservePostRequest) FieldMap(req *http.Request) binding.FieldMap {
	return binding.FieldMap{
		&cf.Name: "name",
		// &cf.Email:   "email",
		// &cf.Message: binding.Field{
		// 	Form:     "message",
		// 	Required: true,
		// },
	}
}

var sessionName = "admin-session"
var store = sessions.NewCookieStore([]byte("something-very-secret"))

func adminFuncMap(w http.ResponseWriter, r *http.Request) template.FuncMap {
	session, _ := store.Get(r, sessionName)
	flashes := session.Flashes()
	err := session.Save(r, w)
	if err != nil {
		fmt.Println(err)
	}
	tm := template.FuncMap{
		"isLoggedIn": func() bool {
			loggedIn := true
			return loggedIn
		},
		"flashMessages": func() []interface{} {
			return flashes
		},
	}
	return tm
}

func (app *AppServer) Handledefault(templatename string, data interface{}, w http.ResponseWriter, r *http.Request) {

	renderPage := func(templateName string, w http.ResponseWriter, req *http.Request, data interface{}) {
		t, err := app.TP.Template(templateName)
		if err != nil {
			render.Render(w, req, ErrInvalidRequest(err))
			return
		}
		log.Println(t)
		t.Funcs(adminFuncMap(w, req)).Execute(w, data)
	}

	renderPage(templatename, w, r, data)

}

func (app *AppServer) RenderPage(geturl string, data_in interface{}, w http.ResponseWriter, r *http.Request) {

	raw_man := `<div id="mainunauth">
  
		<div id="app"> In Main Vue {{ message }}</div>
	 </div> `
	data_ := make(map[string]interface{}, 0)
	raw_tohtml := template.HTML(raw_man)
	data_["unauth"] = "this is unauth variable in data"
	data_["flashMessages"] = "this is flusing"
	data_["mainRaw"] = raw_tohtml

	log.Println("Render Page called", geturl)
	switch geturl {
	case "/default":

		log.Println("default called")
		app.Handledefault("unauth\\index", data_, w, r)
		//render UnAuthorized
	case app.CreateUrl:
		//render CreateHome
		log.Println("Creating has been hit")
		data := app.BuildData(app.CreateUrl, data_in)
		app.Handledefault("create\\index", data, w, r)

	case app.ObserveUrl:
		//render ObserveHome
		log.Println("Observe has been hit")
		data := app.BuildData(app.ObserveUrl, data_in)
		app.Handledefault("observe\\index", data, w, r)

	case app.ObservePostUrl:
		log.Println("observe post hit")
		//	data_in := data

		b_data := app.BuildData(app.ObservePostUrl, data_in)

		app.Handledefault("observe\\index", b_data, w, r)

	//	build_data :=
	case app.ObserveOneUrl:
		//render observe one home
		log.Println("ObserveOne has been hit")
		app.Handledefault("observeone\\index", data_, w, r)
	case app.CollectUrl:
		//render collectHome
	default:
		//render Unauthorized
	}
}

func (app *AppServer) BuildData(url string, indata interface{}) map[string]interface{} {

	out := make(map[string]interface{}, 0)

	log.Println(indata, "received in build data")

	val, ok := indata.(map[string]interface{})

	if !ok {
		log.Println("indata is empty")
	} else {
		for k, v := range val {
			out[k] = v
		}
	}

	switch url {
	case app.ObserveUrl:
		out["url"] = app.AppUrl + app.ObservePostUrl
	case app.ObservePostUrl:
		out["url"] = app.AppUrl + app.ObservePostUrl
		//msgs, _ := app.DB.GetAllObservers()
	case app.CreateUrl:
		out["url"] = app.AppUrl + app.CreateUrl

		///out["observelist"] = msglist

	}

	return out
}
