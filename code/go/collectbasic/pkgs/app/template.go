package app

import (
	"errors"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/Masterminds/sprig"
	"github.com/qobbysam/collectbasic/pkgs/webpack"
)

func viewHelpers() template.FuncMap {
	return template.FuncMap{
		"asset": webpack.AssetHelper,
		"isLoggedIn": func() bool {
			return false
		},
		"flashMessages": func() []interface{} {
			return nil
		},
	}
}

type TemplateManager struct {
	dir       string           // root directory
	ext       string           // extension
	devel     bool             // reload every time
	funcs     template.FuncMap // functions
	loadedAt  time.Time        // loaded at (last loading time)
	layoutDir string           // template layout directory
	templates map[string]*template.Template
}

// NewTemplateManager creates new TemplateManager and loads templates_oild. The dir argument is
// directory to load templates_oild from. The ext argument is extension of
// tempaltes. The devel (if true) turns the TemplateManager to reload templates_oild
// every Render if there is a change in the dir.
func NewTemplateManager(dir, layoutDir string, ext string, funcMap template.FuncMap, devel bool) (tmpl *TemplateManager, err error) {

	// get absolute path
	if dir, err = filepath.Abs(dir); err != nil {
		log.Println("failed to load dirs")
		return
	}

	// get absolute path
	if layoutDir, err = filepath.Abs(layoutDir); err != nil {
		log.Println("failed to get layout dir")
		return
	}

	tmpl = new(TemplateManager)
	tmpl.dir = dir
	tmpl.ext = ext
	tmpl.devel = devel
	tmpl.layoutDir = layoutDir
	tmpl.funcs = funcMap
	//webpack.Init(devel)

	tmpl.templates = make(map[string]*template.Template)

	if err = tmpl.Load(); err != nil {
		log.Println("failed to load in loading here")
		tmpl = nil // drop for GC
	}

	return
}

// Dir returns absolute path to directory with views
func (t *TemplateManager) Dir() string {
	return t.dir
}

// Ext returns extension of views
func (t *TemplateManager) Ext() string {
	return t.ext
}

// Devel returns development pin
func (t *TemplateManager) Devel() bool {
	return t.devel
}

func (t *TemplateManager) Templates() []*template.Template {
	var temps []*template.Template
	for _, v := range t.templates {
		temps = append(temps, v)
	}
	return temps
}

// Funcs sets template functions
func (t *TemplateManager) Funcs(funcMap template.FuncMap) {
	// TODO Implement this next
	//t.Template = t.Template.Funcs(funcMap)
	//t.funcs = funcMap
}

// Load or reload templates_oild
func (t *TemplateManager) Load() (err error) {

	// time point
	t.loadedAt = time.Now()

	layoutFiles, err := filepath.Glob(t.layoutDir + "/*.gohtml")

	if err != nil {
		log.Println("failed loading lay out files")
		return err
	}

	var walkFunc = func(path string, info os.FileInfo, err error) (_ error) {

		// handle walking error if any
		if err != nil {
			log.Println("walk first")
			return err
		}

		// skip all except regular files
		// TODO (kostyarin): follow symlinks
		if !info.Mode().IsRegular() {
			return
		}

		// filter by extension
		if filepath.Ext(path) != t.ext {
			return
		}

		// get relative path
		var rel string
		if rel, err = filepath.Rel(t.dir, path); err != nil {
			log.Println("rel string error")
			return err
		}

		// Ignore files in the layout directory
		if filepath.Dir(path) == t.layoutDir {
			return
		}

		// name of a template is its relative path
		// without extension
		rel = strings.TrimSuffix(rel, t.ext)

		var (
			nt = template.New(rel).Funcs(sprig.FuncMap()).Funcs(t.funcs).Funcs(viewHelpers())
			b  []byte
		)

		if b, err = ioutil.ReadFile(path); err != nil {
			log.Println("error reading file")
			fmt.Println(path)
			return err
		}
		tmpl, err := nt.ParseFiles(layoutFiles...)

		if err != nil {
			log.Println(layoutFiles)
			log.Println("failed to parse files")
			log.Println(err)
			return err
		}

		tmpl, err = nt.Parse(string(b))
		if err != nil {
			log.Println(string(b))
			log.Println(err)
			log.Println(("failed top parse to strig"))
			return err
		}

		t.templates[tmpl.Name()] = tmpl

		return err
	}

	if err = filepath.Walk(t.dir, walkFunc); err != nil {

		log.Println("error during walk func")
		return
	}

	return
}

// IsModified lookups directory for changes to
// reload (or not to reload) templates_oild if development
// pin is true.
func (t *TemplateManager) IsModified() (yep bool, err error) {

	var errStop = errors.New("stop")

	var walkFunc = func(path string, info os.FileInfo, err error) (_ error) {

		// handle walking error if any
		if err != nil {
			return err
		}

		// skip all except regular files
		if !info.Mode().IsRegular() {
			return
		}

		// filter by extension
		if filepath.Ext(path) != t.ext {
			return
		}

		if yep = info.ModTime().After(t.loadedAt); yep == true {
			return errStop
		}

		return
	}

	// clear the errStop
	if err = filepath.Walk(t.dir, walkFunc); err == errStop {
		err = nil
	}

	return
}

func (t *TemplateManager) Template(name string) (tmpl *template.Template, err error) {
	// if development
	if t.devel {

		// lookup directory for changes
		modified, err := t.IsModified()
		if err != nil {
			return nil, err
		}

		// reload
		if modified {
			if err = t.Load(); err != nil {
				return nil, err
			}
		}

	}

	tmpl, ok := t.templates[name]
	if !ok {
		log.Println(t.templates)
		return nil, errors.New("template not found")
	}
	log.Println(tmpl)
	return tmpl.Clone()
}

func (t *TemplateManager) Render(w io.Writer, name string, data interface{}) (err error) {

	tmpl, err := t.Template(name)
	if err != nil {
		return err
	}
	return tmpl.Execute(w, data)
}
