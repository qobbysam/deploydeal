package locdb

import (
	"time"

	"github.com/lithammer/shortuuid"
)

type WorkerObserver struct {
	ID          string `gorm:"primary_key"`
	Name        string
	CreatedTime time.Time
}

type ClientObserver struct {
	ID               string `gorm:"primary_key"`
	MC               string
	Email            string
	WorkerObserverID string
	ClientID         string
	Alive            bool
	CreatedTime      time.Time
}

type ClientSubmission struct {
	ID               string `gorm:"primary_key"`
	Value            string
	CreatedTime      time.Time
	ClientObserverID string
	ClientID         string
}

func NewWorkObserver(name string) *WorkerObserver {
	return &WorkerObserver{
		ID:          shortuuid.New(),
		CreatedTime: time.Now(),
		Name:        name,
	}
}

func NewClientObserver(mc, email, observerid string) *ClientObserver {

	return &ClientObserver{
		ID:               shortuuid.New(),
		MC:               mc,
		Email:            email,
		WorkerObserverID: observerid,
		ClientID:         shortuuid.New(),
		Alive:            true,
	}
}

func NewClientSubmission(value, observerid, clientid string) *ClientSubmission {
	return &ClientSubmission{
		ID:               shortuuid.New(),
		Value:            value,
		ClientObserverID: observerid,
		ClientID:         clientid,
	}
}
