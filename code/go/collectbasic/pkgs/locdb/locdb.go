package locdb

import (
	"fmt"
	"log"

	"github.com/qobbysam/collectbasic/pkgs/config"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

type DBEX struct {
	Name string
	DB   *gorm.DB
}

func NewDBEX(cfg *config.BigConfig) (*DBEX, error) {

	out := DBEX{Name: cfg.BootOpt.DBName}

	db, err := gorm.Open(sqlite.Open(cfg.BootOpt.DBName))

	if err != nil {
		fmt.Println("failed to connect to the server")
		return nil, err
	}

	out.DB = db

	err = out.DoMigrations()

	return &out, err
}

func (ex *DBEX) DoMigrations() error {

	ex.DB.AutoMigrate(&WorkerObserver{}, &ClientObserver{}, &ClientSubmission{})

	return nil
}

func (ex *DBEX) CreateClientObserver() {}

func (ex *DBEX) CreateWorkerObserver(name string) (*WorkerObserver, error) {

	observ := NewWorkObserver(name)

	exx := ex.DB.Create(&observ)

	if exx.Error != nil {
		return nil, exx.Error
	}

	return observ, exx.Error

}

func (ex *DBEX) GetAllObservers() ([]WorkerObserver, error) {

	out := make([]WorkerObserver, 0)

	exx := ex.DB.Order("created_time desc").Order("name").Find(&out)
	//exx := ex.DB.Order(WorkerObserver{CreatedTime: }) Find(&out)

	if exx.Error != nil {
		log.Println("faield to ged,, ", exx.Error)
		return nil, exx.Error
	}

	return out, nil
}

func (ex *DBEX) GetAllCreate() ([]ClientObserver, error) {

	out := make([]ClientObserver, 0)

	exx := ex.DB.Order("created_time desc").Order("mc").Find(&out)
	//exx := ex.DB.Order(WorkerObserver{CreatedTime: }) Find(&out)

	if exx.Error != nil {
		log.Println("faield to ged,, ", exx.Error)
		return nil, exx.Error
	}

	return out, nil
}

func (ex *DBEX) CreateClientSubmission(mc, email, observerid string) (*ClientObserver, error) {

	observ := NewClientObserver(mc, email, observerid)

	exx := ex.DB.Create(&observ)

	if exx.Error != nil {
		return nil, exx.Error
	}

	return observ, exx.Error

}

func (ex *DBEX) GetClientInfo(id string) (*ClientObserver, error) {

	//exx := ex.DB.Order("created_time desc").Order("mc").Find(&out)

	out := ClientObserver{}

	exx := ex.DB.Where(&ClientObserver{ClientID: id}).First(&out)

	if exx.Error != nil {
		return nil, exx.Error
	}

	return &out, exx.Error

}

func (ex *DBEX) UpdateClientInfo(obs ClientObserver) (*ClientObserver, error) {

	//exx := ex.DB.Order("created_time desc").Order("mc").Find(&out)

	//	out := ClientObserver{}

	//exx := ex.DB.Where(&ClientObserver{ClientID: id}).First(&out)

	exx := ex.DB.Save(&obs)
	if exx.Error != nil {
		return nil, exx.Error
	}

	return &obs, exx.Error

}

//func (ex *DBEX)VerifyClientSession()

func (ex *DBEX) BuildClientData(id string) ([]ClientSubmission, error) {

	out := make([]ClientSubmission, 0)

	exx := ex.DB.Where(&ClientSubmission{ClientObserverID: id}).Find(&out)

	if exx.Error != nil {

		return nil, exx.Error
	}

	return out, exx.Error
}
func (ex *DBEX) BuildObserveOneData(id string) ([]ClientSubmission, error) {

	out := make([]ClientSubmission, 0)

	exx := ex.DB.Where(&ClientSubmission{ClientObserverID: id}).Find(&out)

	if exx.Error != nil {

		return nil, exx.Error
	}

	return out, exx.Error
}
func (ex *DBEX) CreateClientCollect(value, observerid, clientid string) (*ClientSubmission, error) {

	observ := NewClientSubmission(value, observerid, clientid)

	exx := ex.DB.Create(&observ)

	if exx.Error != nil {
		return nil, exx.Error
	}

	return observ, exx.Error

}
