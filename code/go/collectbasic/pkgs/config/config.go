package config

type BigConfigProd struct {
	DataLocation string
}

type BootOption struct {
	CreateUrl      string `json:"createUrl"`
	CreatePostUrl  string `json:"createPostUrl"`
	ObserveUrl     string `json:"observeUrl"`
	CollectUrl     string `json:"collectUrl"`
	ClientPostUrl  string `json:"clientPostUrl"`
	ObservePostUrl string `json:"observePostUrl"`
	ObserveOneUrl  string `json:"observeone"`
	DBName         string `json:"dbName"`
	Port           string `json:"port"`
	AppUrl         string `json:"appurl"`
}

type BigConfig struct {
	BootOpt *BootOption `json:"bootoptions"`
	DataLoc string      `json:"dataLoc"`
}

func NewBigConfigProd(path string) *BigConfigProd {

	return &BigConfigProd{
		DataLocation: path,
	}
}

func (cf *BigConfigProd) ProduceBigConfig() (*BigConfig, error) {

	return nil, nil
}
