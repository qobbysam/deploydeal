package internal

import (
	"log"
	"os"
	"path/filepath"

	"encoding/json"
	"fmt"
	"io/ioutil"

	"github.com/qobbysam/collectbasic/pkgs/app"
	"github.com/qobbysam/collectbasic/pkgs/config"
)

type InternalStruct struct {
	App *app.AppServer
}

func (in *InternalStruct) GetConfig(input string) string {

	if input != "" {
		return input
	} else {

		dirpath, _ := os.Executable()

		dir := filepath.Dir(dirpath)

		return dir
	}

}

func (in *InternalStruct) Init(input string) error {
	path := in.GetConfig(input)
	cfg_path := filepath.Join(path, "data", "conf", "conf.json")
	cfg, err := in.BuildConfig(path, cfg_path)

	if err != nil {
		return err
	}

	app := app.NewAppServer(cfg)

	in.App = app

	return nil
}

func (in *InternalStruct) StartServer() {

	err := in.App.StartServer()

	if err != nil {
		fmt.Println("failed to start server, ", err)
	}
	fmt.Println("started rest server successfully")
}

// func (in *InternalStruct) EmailTest() {

// 	err := in.App.SendTest()

// 	if err != nil {
// 		fmt.Println("failed to send test msg,  ", err)
// 	}

// 	fmt.Println("sent test message sucessfully")
// }

func (in *InternalStruct) BuildConfig(basepath, path string) (*config.BigConfig, error) {

	file, err := os.Open(path)

	if err != nil {
		log.Println(path)
		fmt.Println("failed to open file  ,  ", err)
		return nil, err
	}

	by, err := ioutil.ReadAll(file)

	if err != nil {
		fmt.Println("failed read file ,  ", err)
		return nil, err
	}

	var bigConfig config.BigConfig

	err = json.Unmarshal(by, &bigConfig)

	if err != nil {
		fmt.Println("failed to marshal json check order,  ", err)
		return nil, err
	}

	bigConfig.DataLoc = basepath

	return &bigConfig, nil
}

func (in *InternalStruct) StartApplication(action, path string) {

	err := in.Init(path)

	if err != nil {

		fmt.Println("failed to init,  ", err)

		panic("failed to init")
	}

	switch action {
	case "server":
		fmt.Println("starting server")
		in.StartServer()
	// case "emailtest":
	// 	in.EmailTest()

	// case "saveresource":
	// 	in.SaveResource(resourcename, alive)

	// case "cresource":
	// 	in.LoadResource(resourcename)

	//case "turnoff"

	default:
		fmt.Println("not a valid action received")
	}

}

// func (in *InternalStruct) LoadResource(name string) {

// 	err := in.App.DB.LoadResourceMain(name)
// 	if err != nil {
// 		fmt.Println("failed to save cresource ")
// 	} else {
// 		fmt.Println("save client resource run successfully")
// 	}

// }

// func (in *InternalStruct) SaveResource(name string, alive bool) {

// 	err := in.App.DB.SaveReSourceID(name, alive)

// 	if err != nil {
// 		fmt.Println("failed to save ")
// 	} else {
// 		fmt.Println("save sucess")
// 	}

// }

// type InternalStruct struct {
// }

// func (in *InternalStruct) HandleMain(path string) {

// 	to_use := ""

// 	if path != "" {
// 		to_use = path
// 	} else {
// 		osd, _ := os.Executable()

// 		osd_dir := filepath.Dir(osd)

// 		default_path := filepath.Join(osd_dir, "data")

// 		to_use = default_path
// 	}

// 	config := config.NewBigConfigProd(to_use)

// 	_, err := config.ProduceBigConfig()

// 	if err != nil {
// 		log.Println("failed to load big config")
// 		log.Println(err)

// 		panic("Failed to load config")
// 	}
// }
