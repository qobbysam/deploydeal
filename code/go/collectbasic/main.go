package main

import (
	"flag"

	"github.com/qobbysam/collectbasic/internal"
)

func main() {
	start := flag.String("st", "", "start operation, server or emailtest")
	path := flag.String("p", "", "path to data folder")

	flag.Parse()

	internalst := internal.InternalStruct{}

	internalst.StartApplication(*start, *path)
}
