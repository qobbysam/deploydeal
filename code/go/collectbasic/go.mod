module github.com/qobbysam/collectbasic

go 1.17

require (
	github.com/Masterminds/sprig v2.22.0+incompatible
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/render v1.0.1
	github.com/gorilla/sessions v1.2.1
	github.com/lithammer/shortuuid v3.0.0+incompatible
	github.com/mholt/binding v0.3.0
	github.com/pkg/errors v0.9.1
	gorm.io/driver/sqlite v1.3.1
	gorm.io/gorm v1.23.3
)

require (
	github.com/Masterminds/goutils v1.1.1 // indirect
	github.com/Masterminds/semver v1.5.0 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/securecookie v1.1.1 // indirect
	github.com/huandu/xstrings v1.3.2 // indirect
	github.com/imdario/mergo v0.3.12 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.4 // indirect
	github.com/mattn/go-sqlite3 v1.14.9 // indirect
	github.com/mitchellh/copystructure v1.2.0 // indirect
	github.com/mitchellh/reflectwalk v1.0.2 // indirect
	github.com/smartystreets/goconvey v1.7.2 // indirect
	golang.org/x/crypto v0.0.0-20220331220935-ae2d96664a29 // indirect
)
